import React from 'react';
import {useHistory} from "react-router-dom";

function Dashboard({user}) {
    const history = useHistory();
    const handleRegister = (event) => {
        event.preventDefault();
        history.push('/register');
    };
    const handleLogout = (event) => {
        event.preventDefault();
        history.push('/logout');
    };
    return (<div className='Dashboard'>
        <p>Hello, {user.name}</p>
        <div>
            <button onClick={handleRegister}>Register</button>
            <button onClick={handleLogout}>Logout</button>
        </div>
    </div>);
}

export default Dashboard;

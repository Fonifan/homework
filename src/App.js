import './App.css';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Login from './user/presenter/Login';
import Dashboard from "./dashboard/presenter/Dashboard";
import {loginUser, logoutUser, registerUser} from "./user/state/UserAction";
import Logout from "./user/presenter/Logout";
import Register from "./user/presenter/Register";
import UserApi from "./user/service/UserApi";

function App({users}) {
    const userApi = new UserApi();
    return (
        <div className="App">
            <BrowserRouter>
                <Switch>
                    <Route exact path='/'>
                        {isLoggedIn() ? <Dashboard user={getActiveUser()}/> : <Redirect to="/login"/>}
                    </Route>
                    <Route path='/login'>
                        {isLoggedIn() ? <Redirect to='/'/> : <Login userApi={userApi}/>}
                    </Route>
                    <Route path='/logout'>
                        {isLoggedIn() ? <Logout userApi={userApi}/> : <Redirect to="/login"/>}
                    </Route>
                    <Route path='/register'>
                        <Register userApi={userApi}/>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );

    function getActiveUser() {
        return users.activeUser;
    }

    function isLoggedIn() {
        return users.activeUser.token !== null;
    }

}

export default connect(
    (state) => ({
        users: state.users,
    }), {
        loginUser,
        logoutUser,
        registerUser,
    }
)(App);

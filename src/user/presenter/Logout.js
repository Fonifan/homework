import React from "react";
import {connect} from "react-redux";
import {logoutUser} from "../state/UserAction";

function Logout({users, logoutUser, userApi}) {
    const onLogout = async () => {
        await userApi.logout({token: users.activeUser});
        logoutUser();
    };
    return (
        <div>
            <button data-testid='logout-logout' onClick={onLogout}>Logout</button>
        </div>
    );
}


export default connect(
    (state) => ({
        users: state.users,
    }), {
        logoutUser,
    }
)(Logout);


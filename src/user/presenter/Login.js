import React, {useState} from 'react';
import './Login.css';
import {useHistory} from 'react-router-dom';
import {connect} from "react-redux";
import {loginUser} from "../state/UserAction";

function Login({users, loginUser, userApi}) {
    const [name, setName] = useState();
    const [password, setPassword] = useState();
    const history = useHistory();

    const onLogin = async (user) => {
        const response = await userApi.login(user);
        if (isRegistered(user)) {
            loginUser({user, token: response.token});
        } else {
            alert('User not registered');
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        await onLogin({name, password});
    };

    const handleChangeName = (event) => {
        setName(event.target.value);
    };

    const handleChangePassword = (event) => {
        setPassword(event.target.value);
    };
    const handleRegister = (event) => {
        event.preventDefault();
        history.push("/register");
    };

    return (
        <div className='Login'>
            <h1>Log In</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    <p>Username</p>
                    <input data-testid='login-name' type='text' onChange={handleChangeName}/>
                </label>
                <label>
                    <p>Password</p>
                    <input type='password' data-testid='login-password' onChange={handleChangePassword}/>
                </label>
                <div className='LoginButtons'>
                    <button data-testid='login-submit' type='submit'>Submit</button>
                    <button data-testid='login-register' onClick={handleRegister}>Register</button>
                </div>
            </form>
        </div>
    );

    function isRegistered(user) {
        if(!users.registered.hasOwnProperty(user.name)) {
            return false;
        }
        return users.registered[user.name] === user.password;
    }
}

export default connect(
    (state) => ({
        users: state.users,
    }), {
        loginUser,
    }
)(Login);


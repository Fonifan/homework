import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import {connect} from "react-redux";
import {registerUser} from "../state/UserAction";

function Register({registerUser, userApi}) {
    const [name, setName] = useState();
    const [password, setPassword] = useState();
    const history = useHistory();
    const onRegister = async (user) => {
        await userApi.register(user);
        registerUser(user);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        history.push('/');
        await onRegister({name, password});
    };

    const handleChangeName = (event) => {
        setName(event.target.value);
    };

    const handleChangePassword = (event) => {
        setPassword(event.target.value);
    };

    return (
        <div className='Register'>
            <h1>Register</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    <p>Username</p>
                    <input data-testid='register-name' type='text' onChange={handleChangeName}/>
                </label>
                <label>
                    <p>Password</p>
                    <input data-testid='register-password' type='password' onChange={handleChangePassword}/>
                </label>
                <div className='LoginButtons'>
                    <button data-testid='register-submit' type='submit'>Submit</button>
                </div>
            </form>
        </div>
    );
}

export default connect(
    (state) => ({
        users: state.users,
    }), {
        registerUser,
    }
)(Register);

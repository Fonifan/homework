export default class UserApi {
    constructor() {
        this.route = 'http://localhost:8000/user';
    }

    async login(user) {
        const response = await fetch(`${this.route}/login`, this.preparePOSTRequest(user));
        return await response.json();
    }

    async logout(user) {
        const response = await fetch(`${this.route}/logout`, this.preparePOSTRequest(user));
        return await response.json();
    }

    async register(user) {
        const response = await fetch(`${this.route}/register`, this.preparePOSTRequest(user));
        return await response.json();
    }

    preparePOSTRequest(body) {
        return {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        };
    }
}

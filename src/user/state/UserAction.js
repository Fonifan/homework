import {createAction} from '@reduxjs/toolkit';

const moduleName = 'user';

const loginUser = createAction(`@${moduleName}/LOGIN_USER`);
const logoutUser = createAction(`@${moduleName}/LOGOUT_USER`);
const registerUser = createAction(`@${moduleName}/REGISTER_USER`);

export {
    loginUser,
    logoutUser,
    registerUser,
};

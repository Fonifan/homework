import {createReducer} from '@reduxjs/toolkit';
import {loginUser, logoutUser, registerUser} from './UserAction';

const initialState = {
    activeUser: {
        name: null,
        token: null,
    },
    registered: {}
};

const userReducer = createReducer(initialState, {
    [loginUser]: loginUserAction,

    [logoutUser]: logoutUserAction,

    [registerUser]: registerUserAction,
});

function loginUserAction(state, action) {
    const {user, token} = action.payload;
    if (user) {
        state.activeUser = {
            token,
            name: user.name
        }
    }
}

function logoutUserAction(state, action) {
    state.activeUser = {
        token: null,
        name: null,
    };
}

function registerUserAction(state, action) {
    const {name, password} = action.payload;
    state.registered[name] = password;
}

export default userReducer;

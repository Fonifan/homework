export default class MockUserApi {
    async login(user) {
        return new Promise((resolve) => resolve({token: `testToken_${user.name}`}));
    }

    async logout(user) {
        return new Promise((resolve) => resolve());

    }

    async register(user) {
        return new Promise((resolve) => resolve());

    }
}

import React from "react";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import {Provider} from "react-redux";
import store from "../../store";
import {loginUser, registerUser} from "../../user/state/UserAction";
import MockUserApi from "../service/MockUserApi";
import Logout from "../../user/presenter/Logout";
import Register from "../../user/presenter/Register";
import {BrowserRouter} from "react-router-dom";

const name = 'test';
const password = 'password';

beforeEach(() => {
    const userApi = new MockUserApi();
    render(
        <Provider store={store}>
            <BrowserRouter>
                <Register userApi={userApi}/>
            </BrowserRouter>
        </Provider>
    );
});

test('Register page renders', () => {
    const header = screen.getByText(/Register/i);
    expect(header).toBeInTheDocument();

    const nameInput = screen.getByTestId('register-name');
    expect(nameInput).toBeInTheDocument();

    const passInput = screen.getByTestId('register-password');
    expect(passInput).toBeInTheDocument();
});

test('register adds user', async () => {

    const nameInput = screen.getByTestId('register-name');
    const passInput = screen.getByTestId('register-password');
    const submit = screen.getByTestId('register-submit');
    fireEvent.change(nameInput, {target: {value: name}});
    fireEvent.change(passInput, {target: {value: password}});

    fireEvent.click(submit);
    await waitFor(() => screen.getByText('Register'));
    const state = store.getState();
    expect(state.users.registered).toHaveProperty(name);
    expect(state.users.registered.test).toEqual(password);
});


import React from "react";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import {Provider} from "react-redux";
import store from "../../store";
import Login from "../../user/presenter/Login";
import {registerUser} from "../../user/state/UserAction";
import MockUserApi from "../service/MockUserApi";

const name = 'test';
const password = 'test';

beforeEach(() => {
    window.alert = jest.fn();
    const userApi = new MockUserApi();
    render(
        <Provider store={store}>
            <Login userApi={userApi}/>
        </Provider>
    );
});

test('Login page renders', () => {
    const header = screen.getByText(/Log In/i);
    expect(header).toBeInTheDocument();

    const nameInput = screen.getByTestId('login-name');
    expect(nameInput).toBeInTheDocument();

    const passInput = screen.getByTestId('login-password');
    expect(passInput).toBeInTheDocument();
});

test('login fails if user is not registered', async () => {
    window.alert.mockClear();

    const nameInput = screen.getByTestId('login-name');
    const passInput = screen.getByTestId('login-password');
    const submit = screen.getByTestId('login-submit');
    fireEvent.change(nameInput, {target: {value: name}});
    fireEvent.change(passInput, {target: {value: password}});

    fireEvent.click(submit);
    await waitFor(() => screen.getByText('Log In'));
    const state = store.getState();
    expect(state.users.activeUser.name).toBeNull();
    expect(state.users.activeUser.token).toBeNull();
});

test('Valid login', async () => {

    store.dispatch(registerUser({name, password}));

    const nameInput = screen.getByTestId('login-name');
    const passInput = screen.getByTestId('login-password');
    const submit = screen.getByTestId('login-submit');
    fireEvent.change(nameInput, {target: {value: name}});
    fireEvent.change(passInput, {target: {value: password}});

    fireEvent.click(submit);
    await waitFor(() => screen.getByText('Log In'));
    const state = store.getState();
    expect(state.users.activeUser.name).toEqual(name);
    expect(state.users.activeUser.token).toEqual(`testToken_${name}`);
});


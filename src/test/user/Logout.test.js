import React from "react";
import {fireEvent, render, screen, waitFor} from "@testing-library/react";
import {Provider} from "react-redux";
import store from "../../store";
import {loginUser, registerUser} from "../../user/state/UserAction";
import MockUserApi from "../service/MockUserApi";
import Logout from "../../user/presenter/Logout";

const name = 'test';

beforeEach(() => {
    const userApi = new MockUserApi();
    render(
        <Provider store={store}>
            <Logout userApi={userApi}/>
        </Provider>
    );
});

test('Logout page renders', () => {
    const logoutButton = screen.getByTestId('logout-logout');
    expect(logoutButton).toBeInTheDocument();
});

test('logout removes active user', async () => {
    store.dispatch(loginUser({
        user: {
            name
        },
        token: 'testToken',
    }));
    const logoutButton = screen.getByTestId('logout-logout');
    fireEvent.click(logoutButton);
    await waitFor(() => screen.getByTestId('logout-logout'));
    const state = store.getState();
    expect(state.users.activeUser.name).toBeNull();
    expect(state.users.activeUser.token).toBeNull();
});

